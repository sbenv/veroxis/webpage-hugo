FROM registry.gitlab.com/sbenv/veroxis/images/hugo:v0.101.0 as site_generator

COPY . /app
WORKDIR /app
RUN hugo -d dist --minify --cleanDestinationDir

FROM nginxinc/nginx-unprivileged:1.23.1-alpine

COPY --from=site_generator /app/dist /usr/share/nginx/html
