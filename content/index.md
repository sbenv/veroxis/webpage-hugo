+++
isHome = true
+++

# Projects

> I love writing tools which make my life easier. Feel free to [check out my projects on Gitlab](https://gitlab.com/sbenv/veroxis).

## ezd

I've created `ezd` for myself as i was not happy with creating makefiles and shellscripts to orchestrate `docker run` commands.

It is designed to run tasks either in the `shell` or within `docker-seq`.

I have since implemented all my projects buildsystems within it.

I know it is actually used in production in a few companies.

[Releases](https://gitlab.com/sbenv/veroxis/ezd-rs/-/releases) are created for both ARM and x86_64. It works on Linux and MacOS.

> [https://gitlab.com/sbenv/veroxis/ezd-rs](https://gitlab.com/sbenv/veroxis/ezd-rs)

## docker-seq

When using `docker` for building projects locally there is a big caveat in the user management.

Images have to go a few extra miles in order to create files with the current users permissions.

`docker-seq` stands for "docker sequencer".

It allows to run multiple commands in sequence and to use different users for each command.

This way it is possible to:

- use arbitrary docker images
- create a user or install missing packages as root at runtime
- execute commands as the local user

> [https://gitlab.com/sbenv/veroxis/docker-seq](https://gitlab.com/sbenv/veroxis/docker-seq)

## docker-stats-api

When i configured my server i missed the ability to monitor how many resources every container takes.

Luckily i found the `docker stats` command and i decided to create a metrics exporter based on it.

It exposes the `docker stats` metrics through a __REST__ and a __Prometheus__ endpoint.

I've also uploaded my [Grafana Dashboard](https://gitlab.com/sbenv/veroxis/docker-stats-api/-/blob/master/grafana/docker-stats-api.json) within the Repository.

> [https://gitlab.com/sbenv/veroxis/docker-stats-api](https://gitlab.com/sbenv/veroxis/docker-stats-api)

## jeeves

I was getting tired of remembering on which distribution i am.

I was getting tired of remembering how to search for packages and versions in each package manager.

So i wrote `jeeves`.

It abstracts over quite a few distro package managers like `apk`, `pacman` and `apt`.

It also works with `flatpak`.

It provides an interactive package selection for installation and deletion while showing a detailed package description.

> [https://gitlab.com/Veroxis/jeeves](https://gitlab.com/Veroxis/jeeves)

## Images and SDKs

Since both my CI and my local buildsystem depend on having `docker images` i do have quite a lot of repositories for just that.

To avoid [rate-limiting on hub.docker.com](https://docs.docker.com/docker-hub/download-rate-limit/#what-is-the-download-rate-limit-on-docker-hub) i do have mirrors of base images like [alpine](https://gitlab.com/sbenv/veroxis/images/alpine) and [rust](https://gitlab.com/sbenv/veroxis/images/rust).

To have easy cookie-cutter environments i build [SDK images](https://gitlab.com/sbenv/veroxis/images/sdks) in which a lot of regularly used tools and libraries are bundled up.

In order to develop in any of my projects i just need `docker` and `ezd` installed and i'm ready to go.
